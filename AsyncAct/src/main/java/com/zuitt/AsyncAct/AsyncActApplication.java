package com.zuitt.AsyncAct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@SpringBootApplication
public class AsyncActApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsyncActApplication.class, args);
	}

	//	Multiple parameters
//	localhost:8080/welcome?user=value&role=value
	@GetMapping("/welcome")
	public String welcome(@RequestParam("user") String user, @RequestParam("role") String role){
		String roles = role;
		switch (roles) {
			case "admin" :
				return String.format("Welcome back to the class portal, Admin %s ", user);
			case "teacher" :
				return String.format("Thank you for logging in, Teacher %s", user);
			case "student" :
				return String.format("Welcome back to the class portal %s", user);
			default :
				return String.format("Role out of range");
		}
	}


}
