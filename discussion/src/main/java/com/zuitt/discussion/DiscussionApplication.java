package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling http request.
@RestController

public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	//	ctrl + c to stop the server and application

	//	localhost:8080/hello
	@GetMapping("/hello")
//	Maps a get request to the route "/hello" and invokes the method hello().
	public String hello() {
		return "Hello World";

	}

	//	Route with String Query
//	localhost:8080/hi?name=value
	@GetMapping("/hi")
//	"@RequestParam" annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

	//	Multiple parameters
//	localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Joe") String name, @RequestParam(value="friend", defaultValue = "Jane") String friend){
		return  String.format("Hello %s! My name is %s.", friend, name);
	}

	//	Route with path variables
//	Dynamic data is obtained directly from the url
//	localhost:8080/name
	@GetMapping("/hello/{name}")
//	"@PathVariable" annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s!", name);
	}




// Asynchronous Activity
	@GetMapping("/welcome")
//	"@PathVariable" annotation allows us to extract data directly from the URL
	public String welcome(@RequestParam("user") String user, @RequestParam("role") String role){
		String roles = role;
		switch (roles){
			case "admin" :
				return String.format("Welcome back to the class portal, Admin %s", user);
			case "teacher" :
				return String.format("Thank you for logging in, Teacher %s", user);
			case "student" :
				return String.format("Welcome back to the class portal, %s", user);
			default :
				return String.format("Role out of range");
		}
	}

	private ArrayList<Student> students = new ArrayList<Student>();
	@GetMapping("/register")
	public String register(@RequestParam("id") String id, @RequestParam("name") String name, @RequestParam("course") String course){
		Student student = new Student(id, name, course);
		students.add(student);
		return String.format("%s your id number is registered on the system", id);
	}

	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") String id){
		for (Student student : students){
			if (student.getId().equals(id)){
				return String.format("Welcome back %s! You are currently enrolled in s%", student.getName(), student.getCourse());
			}
		}
		return String.format("Your provided %s is not found in the system!",id);
	}



//Activity S09

	ArrayList<String> enrollees = new ArrayList<String>();
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user", defaultValue = "Jose") String user){
		enrollees.add(user);
		return String.format("Thank you for Enrolling, %s", user);
	}

	@GetMapping("/getEnrollees")
	public String Enrollees(){
		return String.format(String.valueOf(enrollees));
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam(value = "name", defaultValue = "Jose") String name, @RequestParam(value = "age", defaultValue = "0") int age){
		return String.format("Hello %s, My age is %d", name, age);
	}

	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id") String id){
		switch (id){
			case "java101" :
				return String.format("Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000.00");
			case "sql101" :
				return String.format("Name: SQL 101, Schedule: TTH 1:00 PM - 04:00 PM, Price: PHP 2000.00");
			case "javaee101" :
				return String.format("Name: Java EE 101, Schedule: MWF 1:00 PM - 04:00 PM, Price: PHP 3500.00");
			default :
				return String.format("Course cannot be found");
		}
	}


}
